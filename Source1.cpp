#pragma once
#include "Pieces.h"
#include "Queen.h"
#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Board.h"
#include "chessException.h"
#include "Rook.h"
#include "King.h"
#include "Knight.h"
#include "Bishop.h"
#include "Pion.h"


void printMat(char** board)
{
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			std::cout << board[i][j] << " ";
		}
		std::cout << endl;
	}
	std::cout << endl;

}

int* stringToMat(string command)
{
	int i = 0;
	int* arr = new int[4];
	for (; i < 4; i++)
	{
		switch (command[i])
		{
		case '8':
		case 'a':
			arr[i] = 0;
			break;
		case '7':
		case 'b':
			arr[i] = 1;
			break;
		case '6':
		case 'c':
			arr[i] = 2;
			break;
		case '5':
		case 'd':
			arr[i] = 3;
			break;
		case '4':
		case 'e':
			arr[i] = 4;
			break;
		case '3':
		case 'f':
			arr[i] = 5;
			break;
		case '2':
		case 'g':
			arr[i] = 6;
			break;
		case '1':
		case 'h':
			arr[i] = 7;
			break;
		default:
			break;
		}
	}
	return arr;
}

vector<Pieces*> removePiece(int shapeNum, vector<Pieces*> oldArr)
{



	int i = 0;
	bool found = false;
	vector<Pieces*> newArr;
	int size = oldArr.size();
	if (shapeNum == 0)
	{
		i = 1;
		found = true;
	}
	for (; i < size - 1; i++)
	{
		if (i != shapeNum)
		{
			newArr.push_back(oldArr[i]);
		}
		else
		{
			found = true;
		}
	}
	if (found)
	{
		newArr.push_back(oldArr[size - 1]);
	}
	return newArr;
}

void ErrorNumber2()
{
	throw sourceSoldier();
}

void ErrorNumber6()
{
	throw wrongMove();
}

int main()
{
	char msgToGraphics[1024];
	Pipe p;
	string ans;
	int* commandMat = new int[4];
	int i = -999, j = i;
	Board b = Board("rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR");
	Rook* r1 = new Rook(0, 0, b._board);
	Rook* r2 = new Rook(0, 7, b._board);
	Rook* r3 = new Rook(7, 0, b._board);
	Rook* r4 = new Rook(7, 7, b._board);
	Knight* n1 = new Knight(0, 1, b._board);
	Knight* n2 = new Knight(0, 6, b._board);
	Knight* n3 = new Knight(7, 1, b._board);
	Knight* n4 = new Knight(7, 6, b._board);
	Bishop* b1 = new Bishop(0, 2, b._board);
	Bishop* b2 = new Bishop(0, 5, b._board);
	Bishop* b3 = new Bishop(7, 2, b._board);
	Bishop* b4 = new Bishop(7, 5, b._board);
	King* k1 = new King(0, 3, b._board);
	King* k2 = new King(7, 3, b._board);
	Queen* q1 = new Queen(0, 4, b._board);
	Queen* q2 = new Queen(7, 4, b._board);
	Pion* p1 = new Pion(1, 0, b._board);
	Pion* p2 = new Pion(1, 1, b._board);
	Pion* p3 = new Pion(1, 2, b._board);
	Pion* p4 = new Pion(1, 3, b._board);
	Pion* p5 = new Pion(1, 4, b._board);
	Pion* p6 = new Pion(1, 5, b._board);
	Pion* p7 = new Pion(1, 6, b._board);
	Pion* p8 = new Pion(1, 7, b._board);
	Pion* p9 = new Pion(6, 0, b._board);
	Pion* p10 = new Pion(6, 1, b._board);
	Pion* p11 = new Pion(6, 2, b._board);
	Pion* p12 = new Pion(6, 3, b._board);
	Pion* p13 = new Pion(6, 4, b._board);
	Pion* p14 = new Pion(6, 5, b._board);
	Pion* p15 = new Pion(6, 6, b._board);
	Pion* p16 = new Pion(6, 7, b._board);
	b._pieces.begin();
	b._pieces.push_back(r1);
	b._pieces.push_back(r2);
	b._pieces.push_back(r3);
	b._pieces.push_back(r4);
	b._pieces.push_back(n1);
	b._pieces.push_back(n2);
	b._pieces.push_back(n3);
	b._pieces.push_back(n4);
	b._pieces.push_back(b1);
	b._pieces.push_back(b2);
	b._pieces.push_back(b3);
	b._pieces.push_back(b4);
	b._pieces.push_back(k1);
	b._pieces.push_back(k2);
	b._pieces.push_back(q1);
	b._pieces.push_back(q2);
	b._pieces.push_back(p1);
	b._pieces.push_back(p2);
	b._pieces.push_back(p3);
	b._pieces.push_back(p4);
	b._pieces.push_back(p5);
	b._pieces.push_back(p6);
	b._pieces.push_back(p7);
	b._pieces.push_back(p8);
	b._pieces.push_back(p9);
	b._pieces.push_back(p10);
	b._pieces.push_back(p11);
	b._pieces.push_back(p12);
	b._pieces.push_back(p13);
	b._pieces.push_back(p14);
	b._pieces.push_back(p15);
	b._pieces.push_back(p16);
	printMat(b._board);

	srand(time_t(NULL));
	bool isConnect = p.connect();
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;
		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			delete[] commandMat;
			delete r2;
			delete r1;
			delete k1;
			delete k2;
			return 0;
		}
	}
	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1"); // just example...
	p.sendMessageToGraphics(msgToGraphics);
	memset(msgToGraphics, 0, sizeof msgToGraphics);
	string msgFromGraphics = p.getMessageFromGraphics();
	while (msgFromGraphics != "quit")
	{
		commandMat = stringToMat(msgFromGraphics);
		i = b.searchPiece(commandMat[1], commandMat[0]);
		try
		{
			if (islower(b._board[commandMat[1]][commandMat[0]]) && b._turn == true)
			{
				ErrorNumber6();
			}
			else if (!(islower(b._board[commandMat[1]][commandMat[0]])) && b._turn == false)
			{
				ErrorNumber6();
			}
			if (i != -999)
			{
				j = b.searchPiece(commandMat[3], commandMat[2]);
				if (b._board[commandMat[3]][commandMat[2]] == 'k' || b._board[commandMat[3]][commandMat[2]] == 'K' && islower(b._board[commandMat[3]][commandMat[2]]) != islower(b._board[commandMat[1]][commandMat[0]]))
				{
					ErrorNumber6();
				}
				if (j != -999 && islower(b._board[commandMat[3]][commandMat[2]]) != islower(b._board[commandMat[1]][commandMat[0]]))
				{
					b._pieces = removePiece(j, b._pieces);
				}
				i = b.searchPiece(commandMat[1], commandMat[0]);
				b._pieces[i]->move(commandMat[0], commandMat[1], commandMat[2], commandMat[3], b._board);
			}
			else
			{
				ErrorNumber2();
			}
		}
		catch (const std::exception & e)
		{
			const char zero = '0';
			const char one = '1';
			if ((e.what())[0] == one || (e.what())[0] == zero)
			{
				if (b._turn)
				{
					b._turn = false;
				}
				else
				{
					b._turn = true;
				}
			}
			std::cout << e.what() << std::endl;
			strcpy_s(msgToGraphics, e.what());
		}
		p.sendMessageToGraphics(msgToGraphics);
		printMat(b._board);
		msgFromGraphics = p.getMessageFromGraphics();
	}
	p.close();
	delete[] commandMat;
	delete r2;
	delete r1;
	delete k1;
	delete k2;
	return 0;
}

