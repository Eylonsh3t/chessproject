#include "Queen.h"

Queen::Queen(int x, int y, char** board) : Pieces(x, y, board)
{
	this->_kingXLocation = 0;
	this->_kingYLocation = 0;
}

Queen::~Queen()
{
}

void Queen::move(int oldX, int oldY, int newX, int newY, char** board)
{
	int end = 0, addVal = 1, startX = 0, startY = 0, start = 0;
	bool goUpOrDown = false; // false = up
	char queen = '#';
	queen = board[oldY][oldX];
	this->checkExceptions(oldX, oldY, newX, newY, board);
	if (newX == oldX && newY != oldY || newY == oldY && newX != oldX)
	{
		if (oldX != newX)
		{
			goUpOrDown = (newX < oldX);
			start = oldX;
			end = newX;
			if (goUpOrDown)
			{
				addVal = -1;
			}
			while (start != end)
			{

				if (this->checkBlockMove(start, newY, start + addVal, newY, board) && (newX != start + addVal))
				{
					board[oldY][start] = '#';
					board[oldY][oldX] = queen;
					throw wrongMove();
				}
				board[oldY][start] = '#';
				start += addVal;
				board[oldY][start] = queen;

			}
		}
		if (oldY != newY)
		{
			goUpOrDown = (newY < oldY);
			start = oldY;
			end = newY;
			if (goUpOrDown)
			{
				addVal = -1;
			}
			while (start != end)
			{

				if (this->checkBlockMove(newX, start, newX, start + addVal, board) && (newY != start + addVal))
				{
					board[start][oldX] = '#';
					board[oldY][oldX] = queen;
					throw wrongMove();
				}
				board[start][newX] = '#';
				start += addVal;
				board[start][newX] = queen;

			}
		}
	}
	else
	{
		if ((newX > oldX&& newY > oldY) || (oldX > newX&& oldY > newY))
		{
			goUpOrDown = (newX < oldX);
			startX = oldX;
			startY = oldY;
			end = newX;
			if (goUpOrDown)
			{
				addVal = -1;
			}
			while (startX != end)
			{

				if (this->checkBlockMove(startX, startY, startX + addVal, startY + addVal, board) && startX + addVal != newX)
				{
					board[startY][startX] = '#';
					board[oldY][oldX] = queen;
					throw wrongMove();
				}
				board[startY][startX] = '#';
				startX += addVal;
				startY += addVal;
				board[startY][startX] = queen;
			}
		}
		else
		{
			goUpOrDown = (newX < oldX);
			startX = oldX;
			startY = oldY;
			end = newX;
			if (goUpOrDown)
			{
				addVal = -1;
			}
			while (startX != end)
			{

				if (this->checkBlockMove(startX, startY, startX + addVal, startY - addVal, board) && startX + addVal != newX)
				{
					board[startY][startX] = '#';
					board[oldY][oldX] = queen;
					throw wrongMove();
				}
				board[startY][startX] = '#';
				startX += addVal;
				startY -= addVal;
				board[startY][startX] = queen;
			}
		}
	}
	this->_y = newX;
	this->_x = newY;
	if (this->checkChess(this->_y, this->_x, board) == 'K' || this->checkChess(this->_y, this->_x, board) == 'k' && islower(this->checkChess(this->_y, this->_x, board)) != islower(board[this->_y][this->_x]))
	{
		throw chess();
	}
	throw okMove();
}

void Queen::checkExceptions(int oldX, int oldY, int newX, int newY, char** currBoard)
{
	if (newY < 0 || newY > 7 || newX < 0 || newX > 7)
	{
		throw wrongIndex();
	}
	if (currBoard[newY][newX] != '#' && islower(currBoard[newY][newX]) == islower(currBoard[oldY][oldX]))
	{
		throw destSoldier();
	}
	if (!(oldX == newX && oldY != newY || oldX != newX && oldY == newY || newX - oldX == oldY - newY || oldX - newX == newY - oldY || oldX - newX == oldY - newY || newX - oldX == newY - oldY))
	{
		throw wrongMove();
	}
	if (currBoard[oldY][oldX] == currBoard[newY][newX])
	{
		throw destSoldier();
	}
}

bool Queen::checkBlockMove(int oldX, int oldY, int newX, int newY, char** currBoard)
{
	return (currBoard[newY][newX] != '#');
}

bool Queen::checkBlockChess(int oldX, int oldY, int newX, int newY, char** currBoard, bool xOrY)
{
	return (currBoard[newY][newX] != '#');
}

char Queen::checkChess(int newX, int newY, char** currBoard)
{
	int endX = 0, startX = 0, endY = 0, startY = 0;
	this->searchForKing(currBoard);
	if (this->_kingXLocation == newX && this->_kingYLocation > newY)
	{
		endY = this->_kingYLocation;
		for (startY = newY + 1; startY <= endY; startY++)
		{
			if (this->checkBlockChess(0, 0, newX, startY, currBoard,true))
			{
				return currBoard[startY][newX];
			}
		}
	}
	if (this->_kingXLocation == newX && this->_kingYLocation < newY)
	{
		endY = this->_kingYLocation;
		for (startY = newY - 1; startY >= endY; startY--)
		{
			if (this->checkBlockChess(0, 0, newX, startY, currBoard, true))
			{
				return currBoard[startY][newX];
			}
		}
	}
	if (this->_kingXLocation > newX && this->_kingYLocation == newY)
	{
		endX = this->_kingXLocation;
		for (startX = newX + 1; startX <= endX; startX++)
		{
			if (this->checkBlockChess(0, 0, startX, newY, currBoard, true))
			{
				return currBoard[newY][startX];
			}
		}
	}
	if (this->_kingXLocation < newX && this->_kingYLocation == newY)
	{
		endX = this->_kingXLocation;
		for (startX = newX - 1; startX >= endX; startX--)
		{
			if (this->checkBlockChess(0, 0, startX, newY, currBoard, true))
			{
				return currBoard[newY][startX];
			}
		}
	}
	if (newX - this->_kingXLocation == newY - this->_kingYLocation)
	{
		startX = newX - 1;
		endX = this->_kingXLocation;
		startY = newY - 1;
		for (; startX >= endX; startX--, startY--)
		{
			if (this->checkBlockChess(0, 0, startX, startY, currBoard, true))
			{
				return currBoard[startY][startX];
			}
		}
	}
	if (this->_kingXLocation - newX == this->_kingYLocation - newY)
	{
		startX = newX + 1;
		endX = this->_kingXLocation;
		startY = newY + 1;
		for (; startX <= endX; startX++, startY++)
		{
			if (this->checkBlockChess(0, 0, startX, startY, currBoard, true))
			{
				return currBoard[startY][startX];
			}
		}
	}
	if (this->_kingXLocation - newX == newY - this->_kingYLocation)
	{
		startX = newX + 1;
		endX = this->_kingXLocation;
		startY = newY - 1;
		for (; startX <= endX; startX++, startY--)
		{
			if (this->checkBlockChess(0, 0, startX, startY, currBoard, true))
			{
				return currBoard[startY][startX];
			}
		}
	}
	if (newX - this->_kingXLocation == this->_kingYLocation - newY)
	{
		startX = newX - 1;
		endX = this->_kingXLocation;
		startY = newY + 1;
		for (; startX >= endX; startX--, startY++)
		{
			if (this->checkBlockChess(0, 0, startX, startY, currBoard, true))
			{
				return currBoard[startY][startX];
			}
		}
	}
	return '\0';
}

void Queen::searchForKing(char** currBoard)
{
	char king = 0;
	if (this->blackOrWhite == "black")
	{
		king = 'K';
	}
	else
	{
		king = 'k';
	}
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			if (currBoard[i][j] == king)
			{
				this->_kingYLocation = i;
				this->_kingXLocation = j;
			}
		}
	}
}
