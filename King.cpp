#include "King.h"
#include "chessException.h"

bool checkNorth(int newX, int newY, char** currBoard)
{
	int i = 0;
	for (; i < 2; i++)
	{
		if (currBoard[newY][newX + i] == 'K' || currBoard[newY][newX + i] == 'k')
		{
			return currBoard[newY][newX + i];
		}
	}
	return '\0';
}

bool checkWest(int newX, int newY, char** currBoard)
{
	int i = 0;
	for (; i < 2; i++)
	{
		if (currBoard[newY + i][newX] == 'K' || currBoard[newY + i][newX] == 'k')
		{
			return currBoard[newY + i][newX];
		}
	}
	return '\0';
}

bool checkEast(int newX, int newY, char** currBoard)
{
	int i = 0;
	for (; i < 2; i++)
	{
		if (currBoard[newY - i][newX] == 'K' || currBoard[newY - i][newX] == 'k')
		{
			return currBoard[newY - i][newX];
		}
	}
	return '\0';
}

bool checkSouth(int newX, int newY, char** currBoard)
{
	int i = 0;
	for (; i < 2; i++)
	{
		if (currBoard[newY][newX - i] == 'K' || currBoard[newY][newX - i] == 'k')
		{
			return currBoard[newY][newX - i];
		}
	}
	return '\0';
}

King::King(int x, int y, char** board) : Pieces(x, y,board)
{
}

King::~King()
{
}

void King::move(int oldX, int oldY, int newX, int newY, char** currBoard)
{
	this->checkExceptions(oldX, oldY, newX, newY, currBoard);

	if (this->blackOrWhite == "black")
	{
		currBoard[newY][newX] = 'k';
	}
	else
	{
		currBoard[newY][newX] = 'K';
	}
	
	this->_y = newX;
	this->_x = newY;
	
	currBoard[oldY][oldX] = '#';
	if (checkChess(this->_y, this->_x, currBoard) != '\0')
	{
		throw chess();
	}
	throw okMove();
}


void King::checkExceptions(int oldX, int oldY, int newX, int newY, char** currBoard)
{
	if (newY < 0 || newY > 7 || newX < 0 || newX > 7)
	{
		throw wrongIndex();
	}
	if (currBoard[newY][newX] != '#' && islower(currBoard[newY][newX]) == islower(currBoard[oldY][oldX]))
	{
		throw destSoldier();
	}
	if (newY > oldY + 1 || newY < oldY - 1 || newX > oldX + 1 || newX < oldX - 1)
	{
		throw wrongMove();
	}
	if (currBoard[oldY][oldX] == currBoard[newY][newX])
	{
		throw destSoldier();
	}
}

bool King::checkBlockMove(int oldX, int oldY, int newX, int newY, char** currBoard)
{
	return false;
}

bool King::checkBlockChess(int oldX, int oldY, int newX, int newY, char** currBoard, bool xOrY)
{
	return false;
}

char King::checkChess(int newX, int newY, char** currBoard)
{
	if (newY == 0 && newX == 0)
	{
		if (checkSouth(newX + 1, newY + 1, currBoard) != '\0' || checkWest(newX + 1, newY + 1, currBoard) != '\0')
		{
			throw chess();
		}
	}
	else if (newY == 7 && newX == 7)
	{
		if (checkNorth(newX - 1, newY - 1, currBoard) != '\0' || checkEast(newX - 1, newY - 1, currBoard) != '\0')
		{
			throw chess();
		}
	}
	else if (newY == 0 && newX == 7)
	{
		if (checkSouth(newX - 1, newY + 1, currBoard) != '\0' || checkWest(newX - 1, newY + 1, currBoard) != '\0')
		{
			throw chess();
		}
	}
	else if (newY == 7 && newX == 0)
	{
		if (checkNorth(newX + 1, newY - 1, currBoard) != '\0' || checkEast(newX + 1, newY - 1, currBoard) != '\0')
		{
			throw chess();
		}
	}
	else if (newY == 0)
	{
		if (checkSouth(newX + 1, newY + 1, currBoard) != '\0' || checkEast(newX + 1, newY + 1, currBoard) != '\0' || checkWest(newX - 1, newY + 1, currBoard) != '\0')
		{
			throw chess();
		}
	}
	else if (newX == 0)
	{
		if (checkNorth(newX + 1, newY - 1, currBoard) != '\0' || checkSouth(newX + 1, newY + 1, currBoard) != '\0' != '\0' || checkEast(newX + 1, newY + 1, currBoard) != '\0')
		{
			throw chess();
		}
	}
	else if (newX == 7)
	{
		if (checkSouth(newX - 1, newY + 1, currBoard) != '\0' || checkNorth(newX - 1, newY - 1, currBoard) != '\0' || checkWest(newX - 1, newY - 1, currBoard) != '\0')
		{
			throw chess();
		}
	}
	else if (newY == 7)
	{
		if (checkEast(newX + 1, newY - 1, currBoard) != '\0' || checkNorth(newX + 1, newY - 1, currBoard) != '\0' || checkWest(newX - 1, newY - 1, currBoard) != '\0')
		{
			throw chess();
		}
	}
	else
	{
		if (checkSouth(newX + 1, newY + 1, currBoard) != '\0' || checkEast(newX + 1, newY + 1, currBoard) != '\0' || checkWest(newX - 1, newY - 1, currBoard) != '\0' || checkNorth(newX - 1, newY - 1, currBoard) != '\0')
		{
			throw chess();
		}
	}
	return '\0';
}

