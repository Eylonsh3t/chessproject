#pragma once
#include <iostream>
#include "Board.h"
#include "King.h"
#include "chessException.h"

class Knight : public Pieces
{
public:
	Knight(int x, int y, char** board);
	~Knight();
	virtual void move(int oldX, int oldY, int newX, int newY, char** board);
	virtual void checkExceptions(int oldX, int oldY, int newX, int newY, char** currBoard);
	virtual bool checkBlockMove(int oldX, int oldY, int newX, int newY, char** currBoard);
	virtual bool checkBlockChess(int oldX, int oldY, int newX, int newY, char** currBoard, bool xOrY);
	virtual char checkChess(int newX, int newY, char** currBoard);
};