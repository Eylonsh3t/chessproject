#pragma once
#include <iostream>
#include "Pieces.h"
using namespace std;
class Board
{
public:
	bool _turn; // true = white. false = black
	Board(string board);
	~Board();
	char** _board;
	int searchPiece(int x, int  y);
	vector<Pieces*> _pieces;
};
