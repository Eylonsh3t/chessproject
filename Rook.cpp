#include "Rook.h"

Rook::Rook(int x, int y, char** board) : Pieces(x, y, board)
{
}

Rook::~Rook()
{
}

void Rook::move(int oldX, int oldY, int newX, int newY, char** board)
{
	int start = 0, end = 0, addVal = 1;
	char rook = '#';
	bool goUpOrDown = false; // false = up
	this->checkExceptions(oldX, oldY, newX, newY, board);
	rook = board[oldY][oldX];
	if (oldX != newX)
	{
		goUpOrDown = (newX < oldX);
		start = oldX;
		end = newX;
		if (goUpOrDown)
		{
			addVal = -1;
		}
		while (start != end)
		{

			if (this->checkBlockMove(start,newY,start + addVal, newY, board) && (newX != start + addVal))
			{
				board[oldY][start] = '#';
				board[oldY][oldX] = rook;
				throw wrongMove();
			}
			board[oldY][start] = '#';
			start += addVal;
			board[oldY][start] = rook;

		}
	}
	if (oldY != newY)
	{
		goUpOrDown = (newY < oldY);
		start = oldY;
		end = newY;
		if (goUpOrDown)
		{
			addVal = -1;
		}
		while (start != end)
		{

			if (this->checkBlockMove(newX,start,newX, start + addVal, board) && (newY != start + addVal))
			{
				board[start][oldX]= '#';
				board[oldY][oldX] = rook;
				throw wrongMove();
			}
			board[start][newX] = '#';
			start += addVal;
			board[start][newX] = rook;

		}
	}
	this->_y = newX;
	this->_x = newY;
	if ((checkChess(this->_y, this->_x, board) == 'k' || checkChess(this->_y, this->_x, board) == 'K') && islower(checkChess(this->_y, this->_x, board)) == islower(checkChess(this->_y, this->_x, board)))
	{
		throw chess();
	}
	throw okMove();
}

void Rook::checkExceptions(int oldX, int oldY, int newX, int newY, char** currBoard)
{
	if (newY < 0 || newY > 7 || newX < 0 || newX > 7)
	{
		throw wrongIndex();
	}
	if (currBoard[newY][newX] != '#' && islower(currBoard[newY][newX]) == islower(currBoard[oldY][oldX]))
	{
		throw destSoldier();
	}
	if (newY != oldY && newX != oldX)
	{
		throw wrongMove();
	}
	if (currBoard[oldY][oldX] == currBoard[newY][newX])
	{
		throw destSoldier();
	}
	if ((currBoard[newY][newX] == 'k' || currBoard[newY][newX] == 'K') && islower(currBoard[newY][newX]) == islower(currBoard[newY][newX]))
	{
		throw wrongChess();
	}
}

bool Rook::checkBlockMove(int oldX, int oldY, int newX, int newY, char** currBoard)
{
	return (currBoard[newY][newX] != '#');
}

bool Rook::checkBlockChess(int oldX, int oldY, int newX, int newY, char** currBoard,bool xOrY)
{
	int i = 0,min = 0,max = 0;
	char _new = currBoard[newY][newX], _old = currBoard[oldY][oldX];
	if (xOrY)
	{
		for (i = std::_Min_value(oldX,newX); i < std::_Max_value(oldX, newX); i++)
		{
			if (currBoard[newY][i] != '#' || currBoard[newY][i] != _new || currBoard[newY][i] != _old)
			{
				return true;
			}
		}
		return false;
	}
	else
	{
		for (i = std::_Min_value(oldY, newY); i < std::_Max_value(oldY, newY); i++)
		{
			if (currBoard[i][newX] != '#' || currBoard[i][newX] != _new || currBoard[i][newX] != _old)
			{
				return true;
			}
		}
		return false;
	}

}

char Rook::checkChess(int newX, int newY, char** currBoard)
{
	int i = 0;
	for (i = 0; i <= 7; i++)
	{
		if (islower(currBoard[newY][newX]) != islower(currBoard[newY][i]) && (currBoard[newY][i] == 'k' || currBoard[newY][i] == 'K'))
		{
			if (!this->checkBlockChess(newX,newY,i,newY,currBoard,true))
			{
				return currBoard[newY][i];
			}
		}
	}
	for (i = 0; i <= 7; i++)
	{
		if (islower(currBoard[newY][newX]) != islower(currBoard[i][newX]) && (currBoard[i][newX] == 'k' || currBoard[i][newX] == 'K'))
		{
			if (this->checkBlockChess(newX, newY, newX, i, currBoard, false))
			{
				return currBoard[i][newX];
			}
		}
	}
	return '\0';
}
