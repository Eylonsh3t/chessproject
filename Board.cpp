#include "Board.h"

Board::Board(string board)
{
	this->_turn = true;
	int i = 0, j = 0;
	this->_board = new char* [8];
	for (i = 0; i < 8; ++i)
		this->_board[i] = new char[8];
	
	for (i = 0 ; i < 8 ; i++)
	{
		this->_board[i][0] = board[(i * 8)];
		for (j = 1; j < 8; j++)
		{
			this->_board[i][j] = board[(i * 8) + j];
		}
	}
	this->_board[7][7] = board[62];
}

Board::~Board()
{
}

int Board::searchPiece(int x, int y)
{
	int i = 0;
	for (; i < this->_pieces.size(); i++)
	{
		if (this->_pieces[i]->_x == x && this->_pieces[i]->_y == y)
		{
			return i;
		}
	}
	return -999;
}


