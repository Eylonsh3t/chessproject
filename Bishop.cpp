#include "Bishop.h"

Bishop::Bishop(int x, int y, char** board) : Pieces(x, y, board)
{
}

Bishop::~Bishop()
{
}


void Bishop::move(int oldX, int oldY, int newX, int newY, char** board)
{
	int  end = 0, addVal = 1,startX = 0,startY = 0;
	bool goUpOrDown = false; // false = up
	char bishop = '#';
	bishop = board[oldY][oldX];
	checkExceptions(oldX,oldY,newX,newY,board);
	if ((newX > oldX && newY > oldY) || (oldX > newX && oldY > newY))
	{
		goUpOrDown = (newX < oldX);
		startX = oldX;
		startY = oldY;
		end = newX;
		if (goUpOrDown)
		{
			addVal = -1;
		}
		while (startX != end)
		{

			if (this->checkBlockMove(startX, startY, startX + addVal, startY + addVal, board) && startX + addVal != newX)
			{
				board[startY][startX] = '#';
				board[oldY][oldX] = bishop;
				throw wrongMove();
			}
			board[startY][startX] = '#';
			startX += addVal;
			startY += addVal;
			board[startY][startX] = bishop;
		}
	}
	else
	{
		goUpOrDown = (newX < oldX);
		startX = oldX;
		startY = oldY;
		end = newX;
		if (goUpOrDown)
		{
			addVal = -1;
		}
		while (startX != end)
		{

			if (this->checkBlockMove(startX,startY,startX + addVal, startY - addVal, board) && startX + addVal != newX)
			{
				board[startY][startX] = '#';
				board[oldY][oldX] = bishop;
				throw wrongMove();
			}
			board[startY][startX] = '#';
			startX += addVal;
			startY -= addVal;
			board[startY][startX] = bishop;
		}
	}
	this->_y = newX;
	this->_x = newY;
	if ((checkChess(this->_y, this->_x, board) == 'k' || checkChess(this->_y, this->_x, board) == 'K') && islower(checkChess(this->_y, this->_x, board)) == islower(checkChess(this->_y, this->_x, board)))
	{
		throw chess();
	}
	throw okMove();
}

void Bishop::checkExceptions(int oldX, int oldY, int newX, int newY, char** currBoard)
{
	if (newY < 0 || newY > 7 || newX < 0 || newX > 7)
	{
		throw wrongIndex();
	}
	if (currBoard[newY][newX] != '#' && islower(currBoard[newY][newX]) == islower(currBoard[oldY][oldX]))
	{
		throw destSoldier();
	}
	if (!(newX - oldX == oldY - newY || oldX - newX == newY - oldY || oldX - newX == oldY - newY || newX - oldX == newY - oldY))
	{
		throw wrongMove();
	}
	if (currBoard[oldY][oldX] == currBoard[newY][newX])
	{
		throw destSoldier();
	}
	if ((currBoard[newY][newX] == 'k' || currBoard[newY][newX] == 'K') && islower(currBoard[newY][newX]) == islower(currBoard[newY][newX]))
	{
		throw wrongChess();
	}
}

bool Bishop::checkBlockMove(int oldX, int oldY, int newX, int newY, char** currBoard)
{
	return (currBoard[newY][newX] != '#');
}

bool Bishop::checkBlockChess(int oldX, int oldY, int newX, int newY, char** currBoard, bool xOrY)
{
	return ((currBoard[newY][newX] == 'K' || currBoard[newY][newX] == 'k')  && islower(currBoard[newY][newX]) != islower(currBoard[oldY][oldX]));
}

char Bishop::checkChess(int newX, int newY, char** currBoard)
{
	int x = newX, y = newY;
	while (x <= 7 && y <= 7)
	{
		if (checkBlockChess(newX, newY, x, y, currBoard, true))
		{
			return currBoard[y][x];
		}
		x++;
		y++;
	} 
	x = newX;
	y = newY;
	while (x >= 0 && y >= 0)
	{
		if (checkBlockChess(newX, newY, x, y, currBoard, true))
		{
			return currBoard[y][x];
		}
		x--;
		y--;
	}
	x = newX;
	y = newY;
	while (x >= 0 && y <= 7)
	{
		if (checkBlockChess(newX, newY, x, y, currBoard, true))
		{
			return currBoard[y][x];
		}
		x--;
		y++;
	}
	x = newX;
	y = newY;
	while (x <= 7 && y >= 0)
	{
		if (checkBlockChess(newX, newY, x, y, currBoard, true))
		{
			return currBoard[y][x];
		}
		x++;
		y--;
	}
	return '#';
}
