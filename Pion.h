#pragma once
#include "Pieces.h"
class Pion : public Pieces
{
private:
	bool firstMove;
public:
	Pion(int x, int y, char** board);
	~Pion();
	virtual void move(int oldX, int oldY, int newX, int newY, char** board);
	virtual void checkExceptions(int oldX, int oldY, int newX, int newY, char** currBoard);
	virtual bool checkBlockMove(int oldX, int oldY, int newX, int newY, char** currBoard);
	virtual bool checkBlockChess(int oldX, int oldY, int newX, int newY, char** currBoard, bool xOrY);
	virtual char checkChess(int newX, int newY, char** currBoard);
};