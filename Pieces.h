#pragma once
#include <iostream>
#include <vector>
#include <math.h>
#include "chessException.h"
using namespace std;
class Pieces
{
public:
	Pieces(int x, int y, char** board);
	~Pieces();
	virtual void move(int oldX, int oldY, int newX, int newY, char** board) = 0;
	int _x;
	int _y;
	virtual void checkExceptions(int oldX, int oldY, int newX, int newY, char** currBoard) = 0;
	virtual bool checkBlockMove(int oldX, int oldY, int newX, int newY, char** currBoard) = 0;
	virtual bool checkBlockChess(int oldX, int oldY, int newX, int newY, char** currBoard, bool xOrY) = 0;
	virtual char checkChess(int newX, int newY, char** currBoard) = 0;
protected:
	string blackOrWhite;
};
