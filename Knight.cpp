#include "Knight.h"

Knight::Knight(int x, int y, char** board) : Pieces(x, y, board)
{
}

Knight::~Knight()
{
}

void Knight::move(int oldX, int oldY, int newX, int newY, char** board)
{
	this->checkExceptions(oldX, oldY, newX, newY, board);
	if (this->blackOrWhite == "black")
	{
		board[newY][newX] = 'g';
	}
	else
	{
		board[newY][newX] = 'G';
	}

	this->_y = newX;
	this->_x = newY;

	board[oldY][oldX] = '#';
	if (checkChess(this->_y, this->_x, board) != '\0')
	{
		throw chess();
	}
	throw okMove();
}

void Knight::checkExceptions(int oldX, int oldY, int newX, int newY, char** currBoard)
{
	if (newY < 0 || newY > 7 || newX < 0 || newX > 7)
	{
		throw wrongIndex();
	}
	if (currBoard[newY][newX] != '#' && islower(currBoard[newY][newX]) == islower(currBoard[oldY][oldX]))
	{
		throw destSoldier();
	}
	if (!(oldY - 1 == newY && oldX - 2 == newX || oldY + 1 == newY && oldX - 2 == newX || oldY - 2 == newY && oldX - 1 == newX || oldY + 2 == newY && oldX - 1 == newX || oldY - 2 == newY && oldX + 1 == newX || oldY + 2 == newY && oldX + 1 == newX || oldY - 1 == newY && oldX + 2 == newX || oldY + 1 == newY && oldX + 2 == newX))
	{
		throw wrongMove();
	}
	if (currBoard[oldY][oldX] == currBoard[newY][newX])
	{
		throw destSoldier();
	}
}

bool Knight::checkBlockMove(int oldX, int oldY, int newX, int newY, char** currBoard)
{
	return false;
}

bool Knight::checkBlockChess(int oldX, int oldY, int newX, int newY, char** currBoard, bool xOrY)
{
	return false;
}

char Knight::checkChess(int newX, int newY, char** currBoard)
{
	if (newX == 8 && newY == 8)
	{
		if (this->blackOrWhite == "black")
		{
			if (currBoard[newY - 1][newX - 2] == 'K')
			{
				return 1;
			}
			if (currBoard[newY - 2][newX - 1] == 'K')
			{
				return 1;
			}
		}
		else
		{
			if (currBoard[newY - 1][newX - 2] == 'k')
			{
				return 1;
			}
			if (currBoard[newY - 2][newX - 1] == 'k')
			{
				return 1;
			}
		}
	}
	else if (newX == 8)
	{
		if (this->blackOrWhite == "black")
		{
			if (currBoard[newY - 1][newX - 2] == 'K')
			{
				return 1;
			}
			if (currBoard[newY + 1][newX - 2] == 'K')
			{
				return 1;
			}
			if (currBoard[newY - 2][newX - 1] == 'K')
			{
				return 1;
			}
			if (currBoard[newY + 2][newX - 1] == 'K')
			{
				return 1;
			}
		}
		else
		{
			if (currBoard[newY - 1][newX - 2] == 'k')
			{
				return 1;
			}
			if (currBoard[newY + 1][newX - 2] == 'k')
			{
				return 1;
			}
			if (currBoard[newY - 2][newX - 1] == 'k')
			{
				return 1;
			}
			if (currBoard[newY + 2][newX - 1] == 'k')
			{
				return 1;
			}
		}
	}
	else if (newY == 8)
	{
		if (this->blackOrWhite == "black")
		{
			if (currBoard[newY - 1][newX - 2] == 'K')
			{
				return 1;
			}
			if (currBoard[newY - 2][newX - 1] == 'K')
			{
				return 1;
			}
			if (currBoard[newY - 2][newX + 1] == 'K')
			{
				return 1;
			}
			if (currBoard[newY - 1][newX + 2] == 'K')
			{
				return 1;
			}
		}
		else
		{
			if (currBoard[newY - 1][newX - 2] == 'k')
			{
				return 1;
			}
			if (currBoard[newY - 2][newX - 1] == 'k')
			{
				return 1;
			}
			if (currBoard[newY - 2][newX + 1] == 'k')
			{
				return 1;
			}
			if (currBoard[newY - 1][newX + 2] == 'k')
			{
				return 1;
			}
		}
	}
	else if (newX == 0 && newY == 0)
	{
		if (this->blackOrWhite == "black")
		{
			if (currBoard[newY + 2][newX + 1] == 'K')
			{
				return 1;
			}
			if (currBoard[newY + 1][newX + 2] == 'K')
			{
				return 1;
			}
		}
		else
		{
			if (currBoard[newY + 2][newX + 1] == 'k')
			{
				return 1;
			}
			if (currBoard[newY + 1][newX + 2] == 'k')
			{
				return 1;
			}
		}
	}
	else if (newX == 0)
	{
		if (this->blackOrWhite == "black")
		{
			if (currBoard[newY - 2][newX + 1] == 'K')
			{
				return 1;
			}
			if (currBoard[newY + 2][newX + 1] == 'K')
			{
				return 1;
			}
			if (currBoard[newY - 1][newX + 2] == 'K')
			{
				return 1;
			}
			if (currBoard[newY + 1][newX + 2] == 'K')
			{
				return 1;
			}
		}
		else
		{
			if (currBoard[newY - 2][newX + 1] == 'k')
			{
				return 1;
			}
			if (currBoard[newY + 2][newX + 1] == 'k')
			{
				return 1;
			}
			if (currBoard[newY - 1][newX + 2] == 'k')
			{
				return 1;
			}
			if (currBoard[newY + 1][newX + 2] == 'k')
			{
				return 1;
			}
		}
	}
	else if (newY == 0)
	{
		if (this->blackOrWhite == "black")
		{
			if (currBoard[newY + 1][newX - 2] == 'K')
			{
				return 1;
			}
			if (currBoard[newY + 2][newX - 1] == 'K')
			{
				return 1;
			}
			if (currBoard[newY + 2][newX + 1] == 'K')
			{
				return 1;
			}
			if (currBoard[newY + 1][newX + 2] == 'K')
			{
				return 1;
			}
		}
		else
		{
			if (currBoard[newY + 1][newX - 2] == 'k')
			{
				return 1;
			}
			if (currBoard[newY + 2][newX - 1] == 'k')
			{
				return 1;
			}
			if (currBoard[newY + 2][newX + 1] == 'k')
			{
				return 1;
			}
			if (currBoard[newY + 1][newX + 2] == 'k')
			{
				return 1;
			}
		}
	}
	else
	{
		if (this->blackOrWhite == "black")
		{
			if (currBoard[newY - 1][newX - 2] == 'K')
			{
				return 1;
			}
			if (currBoard[newY + 1][newX - 2] == 'K')
			{
				return 1;
			}
			if (currBoard[newY - 2][newX - 1] == 'K')
			{
				return 1;
			}
			if (currBoard[newY + 2][newX - 1] == 'K')
			{
				return 1;
			}
			if (currBoard[newY - 2][newX + 1] == 'K')
			{
				return 1;
			}
			if (currBoard[newY + 2][newX + 1] == 'K')
			{
				return 1;
			}
			if (currBoard[newY - 1][newX + 2] == 'K')
			{
				return 1;
			}
			if (currBoard[newY + 1][newX + 2] == 'K')
			{
				return 1;
			}
		}
		else
		{
			if (currBoard[newY - 1][newX - 2] == 'k')
			{
				return 1;
			}
			if (currBoard[newY + 1][newX - 2] == 'k')
			{
				return 1;
			}
			if (currBoard[newY - 2][newX - 1] == 'k')
			{
				return 1;
			}
			if (currBoard[newY + 2][newX - 1] == 'k')
			{
				return 1;
			}
			if (currBoard[newY - 2][newX + 1] == 'k')
			{
				return 1;
			}
			if (currBoard[newY + 2][newX + 1] == 'k')
			{
				return 1;
			}
			if (currBoard[newY - 1][newX + 2] == 'k')
			{
				return 1;
			}
			if (currBoard[newY + 1][newX + 2] == 'k')
			{
				return 1;
			}
		}
	}
		return '\0';
}
