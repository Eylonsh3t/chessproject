#pragma once
#include <exception>
class okMove : public std::exception
{
	virtual const char* what() const
	{
		//return "You don't have any soldier in that source square\n";
		return "0" + NULL;
	}
};


class chess : public std::exception
{
	virtual const char* what() const
	{
		//return "You don't have any soldier in that source square\n";
		return "1" + NULL;
	}
};

class sourceSoldier : public std::exception
{
	virtual const char* what() const
	{
		//return "You don't have any soldier in that source square\n";
		return "2" + NULL;
	}
};

class destSoldier : public std::exception
{
	virtual const char* what() const
	{
		//return "There is soldier in that destination square so you cant move to there\n";
		return "3" + NULL;
	}
};

class wrongChess : public std::exception
{
	virtual const char* what() const
	{
		//return "This move causing wrong chess\n";
		return "4" + NULL;
	}
};

class wrongIndex : public std::exception
{
	virtual const char* what() const
	{
		//return "The square index is wrong\n";
		return "5" + NULL;
	}
};

class wrongMove : public std::exception
{
	virtual const char* what() const
	{
		//return "There is an wrong move\n";
		return "6" + NULL;
	}
};

class sourceDestSame : public std::exception
{
	virtual const char* what() const
	{
		//return "The source and the destination are the same index\n";
		return "7" + NULL;
	}
};

class mat : public std::exception
{
	virtual const char* what() const
	{
		//return "There is a chess the game is over\n";
		return "8" + NULL;
	}
};