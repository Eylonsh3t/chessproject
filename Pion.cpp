#include "Pion.h"

Pion::Pion(int x, int y, char** board) : Pieces(x,y,board)
{
	this->firstMove = true;
}

Pion::~Pion()
{
}

void Pion::move(int oldX, int oldY, int newX, int newY, char** board)
{
	char pion = board[oldY][oldX];
	this->checkExceptions(oldX,oldY,newX,newY,board);
	board[oldY][oldX] = '#';
	board[newY][newX] = pion;
	this->firstMove = false;
	this->_y = newX;
	this->_x = newY;
	if (this->checkChess(newX,newY,board) == 'k' || this->checkChess(newX, newY, board) == 'K')
	{
		throw chess();
	}
	throw okMove();
}

void Pion::checkExceptions(int oldX, int oldY, int newX, int newY, char** currBoard)
{
	if (newY < 0 || newY > 7 || newX < 0 || newX > 7)
	{
		throw wrongIndex();
	}
	if (currBoard[newY][newX] != '#' && islower(currBoard[newY][newX]) == islower(currBoard[oldY][oldX]))
	{
		throw destSoldier();
	}
	if (this->blackOrWhite == "black")
	{
		if (((newY - oldY == 2) && !this->firstMove) || (newY - oldY == 1 && newX - oldX == 0 && currBoard[newY][newX] != '#') || (newY - oldY != 1 && (newY - oldY != 2)))
		{
			throw wrongMove();
		}
		else if ((newY - oldY == 1 && (newX - oldX == -1 || newX - oldX == 1) && currBoard[newY][newX] == '#'))
		{
			throw wrongMove();
		}
	}
	else
	{

		if (((newY - oldY == -2) && !this->firstMove)  || (newY - oldY == -1 && newX - oldX == 0 && currBoard[newY][newX] != '#') || (newY - oldY != -1 && (newY - oldY != -2)))
		{
			throw wrongMove();
		}
		else if ((newY - oldY == -1 && (newX - oldX == -1 || newX - oldX == 1) && currBoard[newY][newX] == '#'))
		{
			throw wrongMove();
		}
	}
	if (currBoard[oldY][oldX] == currBoard[newY][newX])
	{
		throw destSoldier();
	}
	if ((currBoard[newY][newX] == 'k' || currBoard[newY][newX] == 'K') && islower(currBoard[newY][newX]) == islower(currBoard[newY][newX]))
	{
		throw wrongChess();
	}
}

bool Pion::checkBlockMove(int oldX, int oldY, int newX, int newY, char** currBoard)
{
	return false;
}

bool Pion::checkBlockChess(int oldX, int oldY, int newX, int newY, char** currBoard, bool xOrY)
{
	return false;
}

char Pion::checkChess(int newX, int newY, char** currBoard)
{
	if (this->blackOrWhite == "black")
	{
		if (newY == 7)
		{
			return '\0';
		}
		else if (newX == 7)
		{
			if ((currBoard[newY + 1][newX - 1] == 'k' || currBoard[newY + 1][newX - 1] == 'K') && islower(currBoard[newY + 1][newX - 1]) != islower(currBoard[newY][newX]))
			{
				return currBoard[newY + 1][newX - 1];
			}
		}
	}
	else
	{
		if (newY == 0)
		{
			return '\0';
		}
		else if (newX == 0)
		{
			if ((currBoard[newY + 1][newX - 1] == 'k' || currBoard[newY + 1][newX - 1] == 'K') && islower(currBoard[newY + 1][newX - 1]) != islower(currBoard[newY][newX]))
			{
				return currBoard[newY + 1][newX - 1];
			}
		}
	}
	if ((currBoard[newY + 1][newX - 1] == 'k' || currBoard[newY + 1][newX - 1] == 'K') && islower(currBoard[newY + 1][newX - 1]) != islower(currBoard[newY][newX]))
	{
		return currBoard[newY + 1][newX - 1];
	}
	if ((currBoard[newY + 1][newX + 1] == 'k' || currBoard[newY + 1][newX + 1] == 'K') && islower(currBoard[newY + 1][newX + 1]) != islower(currBoard[newY][newX]))
	{
		return currBoard[newY + 1][newX + 1];
	}
	return '\0';
}
